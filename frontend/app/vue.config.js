module.exports = {
   baseUrl:'/frontend/app/dist',
   filenameHashing:false,
   chainWebpack: config =>  {
        config.plugins.delete('optimize-css')
   }	
}
