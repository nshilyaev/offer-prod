import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        products: [],
        one_product: [],
        prod_default:[],
        all_data: [],
        template: [],
        order:[],
        csrf:"",
        user:[],
        offers:[],
        selectedOffer:[],
        sale:0,
        url:"http://offer/",
        quants:[],
        searched:[],
        order_validate:[],
        order_back:[],
        toggleprice_trigger:false,
        valuta:'р.',
    },
    mutations: {
        togglePrice(state,data){   
            if(!state.toggleprice_trigger){ 
                var nodollar;
                state.order_back = JSON.parse(JSON.stringify(state.order))
                for(let item of state.order){
                    item.price_roznica = item.dollar_price 
                    item.valuta = '$'
                }
                state.order =  state.order.filter(function (item) {
                    return item.price_roznica !== '0'
                })
                state.toggleprice_trigger = true
                state.valuta = '$'
            }else{
                state.order = []
                for(let backitem of state.order_back){
                    state.order.push(backitem)
                }
                state.toggleprice_trigger = false
                state.valuta ='р.'
            }
        },
        products(state, data) {
            state.products = data
            state.prod_default = data
        },

        one_product(state, data) {
            state.one_product = data
        },

        all_data(state, data) {
            state.all_data = data
        },

        current(state, data) {
            state.template = data.template
        },

        order(state,data){      
           
            if(state.order.length<1 || data.quant == 0){
                state.order.push(data)    
                data.quant = 1;  
            }else{
                data.quant = parseInt(data.quant)+1;  
            }
            
        },
        delete_item(state,data){
            state.order =  state.order.filter(function (item) {
                return item !== data
            })
        },
        csrf(state,data){
            state.csrf = data;
        },
        user(state,data){
            state.user = data;
        },
        selectedOffer(state,data){
            var prods = JSON.parse(data[0].data);
            var temp_id = JSON.parse(data[0].template_id);
            var selectTemplate;
           
            state.selectedOffer = data;
            state.order = [];    

            for(let prod of prods){
                state.order.push(prod)
            }

            selectTemplate = state.all_data.templates.filter(function(template){
                    return template.id == temp_id;
            })
              
            state.template = [];
            state.template = selectTemplate[0];
            state.sale = data[0].main_sale; 
        }
    },
    actions: {
        toggleprice:({commit}) =>{
            commit('togglePrice');
        },
        selectCart:({commit},offer) => {
            commit('selectedOffer',offer);
        },
        set_user:({commit}, user) =>{
            commit('user',user);
        },
        get_csrf:({commit,state}) =>{
            fetch(state.url+'backend/web/api-csrf', {
                headers: {
                    'Access-Control-Allow-Origin': "*"
                },
            }).then(function (response) {
                return response.json();
                }).then(function (data) {
                    commit('csrf', data);
            });    
        },
        set_products: ({commit,state}) => {
            fetch(state.url+'backend/web/api-getprods', {
                headers: {
                    'Access-Control-Allow-Origin': "*"
                },
            })
                .then(function (response) {
                    return response.json();
                })
                .then(function (data) {
                    commit('products', data);
                });
        },
        set_one_product: ({commit}, id_prod) => {
            commit('one_product', id_prod)
        },
        set_all_data: ({commit,state},data) => {
            fetch(state.url+'backend/web/api-all', {
                method:'put',
                headers: {
                    'Access-Control-Allow-Origin': "*"
                },
                body:'&user='+data,
            })
                .then(function (response) {
                    return response.json()
                })
                .then(function (data) {
                    commit('all_data', data);
                });
        },
        set_template_current: ({commit}, template) => {
            commit({
                type: 'current',
                template: template
            })
        },

        set_order: ({commit}, order) => {
            commit('order', order)
        },
        delete_item:({commit},item)=>{
            commit('delete_item', item)
        },
        filter_products:({commit},data)=>{
          commit('filter_products',data)
        },
    },
    getters: {
        get_products: state => {
            return state.products
        },
        get_product: state => {
            let item;
            for (let prod of state.products) {
                if (prod.id == state.one_product) {
                    item = prod
                }
            }

            return item
        },
        get_all_data: state => {
            return state.all_data
        },
    }
})
