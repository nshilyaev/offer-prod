<?php

namespace backend\models;

use common\models\User;
use common\models\LoginForm;
use yii\data\ActiveDataProvider;
use backend\templates\Templates;
use Yii;

class Users extends User
{
    public static function getUsersList(){
        $users_data = new ActiveDataProvider([
            'query' => self::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $users_data;
    }

    public static function UsersForApi(){
        $data = self::find()->asArray()->all();
        return $data;
    }

    public function getTemplates(){
        return $this->hasMany(Templates::className(), ['id' => 'templates_id'])
            ->viaTable('user_templates', ['user_id' => 'id']);
    }

    public static function CustomLoginIn($post){
        $login = new LoginForm();
        $login->loginApp($post);
        return  $login->loginApp($post);
    }
}