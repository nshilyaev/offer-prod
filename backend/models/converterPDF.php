<?php
/**
 * Created by PhpStorm.
 * User: huge
 * Date: 04.12.18
 * Time: 17:59
 */

namespace backend\models;

use Yii;

class converterPDF
{
    public function convert($post){
        $test = [];
        // return $pdf->render();
        foreach ($post as $p){
            array_push($test,$p);
        }

        $pdf = Yii::$app->pdf;

        $mpdf = $pdf->api;

        $stylesheet = file_get_contents('../../frontend/app/dist/css/app.css');
        $filename = md5($test[0]);

        $mpdf->WriteHTML($stylesheet,1);
        $mpdf->WriteHTML($test[0],2);
        $mpdf->list_indent_first_level = 0;
        $mpdf->SetHeader('');
        $mpdf->Output('../../uploads/pdf/'.$filename.'.pdf', 'F'); //

//        $pdf = new Pdf([
//            // set to use core fonts only
//            'mode' => Pdf::MODE_BLANK,
//            // A4 paper format
//            'format' => Pdf::FORMAT_A4,
//            // portrait orientation
//            'orientation' => Pdf::ORIENT_PORTRAIT,
//            // stream to browser inline
//            'destination' => Pdf::DEST_FILE,
//            // your html content input
//            'content' => $test[0],
//            // format content from your own css file if needed or use the
//            // enhanced bootstrap css built by Krajee for mPDF formatting
//            'cssFile' => '../../frontend/app/dist/css/app.css',
//            // any css to be embedded if required
//            'cssInline' => '',
//            // set mPDF properties on the fly
//            'options' => ['title' => 'Krajee Report Title'],
//            // call mPDF methods on the fly
//            'methods' => [
//                'SetHeader'=>[''],
//                'SetFooter'=>[''],
//            ]
//        ]);
//
//
//        $pdf->filename = '../../uploads/pdf/test2.pdf';
//
//        $pdf->render();

        return '/uploads/pdf/'.$filename.'.pdf';
    }
}