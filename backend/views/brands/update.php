<div class="container">
    <div class="row">
        <div class="col-12">
            <h2>Бренд <?=$data['name']?></h2>
            <form action="/backend/web/change" method="POST">
                <label for="">Название</label>   
                <div class="clearfix"></div>
                <input type="text" name="name" value="<?=$data['name']?>">
                <div class="clearfix"></div>
                <input type="submit" class="btn btn-success mt-3" value="Изменить">
                <input type="hidden" name="_csrf-backend" value="<?=$csrf?>" />
                <input type="hidden" name="id" value="<?=$data['id']?>">
                <div class="clearfix"></div>
                <a class="btn btn-primary mt-3" href="/backend/web/brands">Назад</a>
            </form>
        </div>
    </div>
</div>