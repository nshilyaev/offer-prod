<?php

use yii\db\Migration;

/**
 * Handles the creation of table `offers`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `templates`
 */
class m190104_100337_create_offers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('offers', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'template_id' => $this->integer(),
            'data' => $this->string(20000),
            'main_sale' => $this->integer(),
            'name' => $this->string(100),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-offers-user_id',
            'offers',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-offers-user_id',
            'offers',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `template_id`
        $this->createIndex(
            'idx-offers-template_id',
            'offers',
            'template_id'
        );

        // add foreign key for table `templates`
        $this->addForeignKey(
            'fk-offers-template_id',
            'offers',
            'template_id',
            'templates',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-offers-user_id',
            'offers'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-offers-user_id',
            'offers'
        );

        // drops foreign key for table `templates`
        $this->dropForeignKey(
            'fk-offers-template_id',
            'offers'
        );

        // drops index for column `template_id`
        $this->dropIndex(
            'idx-offers-template_id',
            'offers'
        );

        $this->dropTable('offers');
    }
}
