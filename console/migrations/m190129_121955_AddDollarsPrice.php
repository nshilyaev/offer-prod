<?php

use yii\db\Migration;

/**
 * Class m190129_121955_AddDollarsPrice
 */
class m190129_121955_AddDollarsPrice extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products','dollar_price','integer DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->removeColumn('products','dollar_price');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190129_121955_AddDollarsPrice cannot be reverted.\n";

        return false;
    }
    */
}
