<?php

use yii\db\Migration;

/**
 * Class m181227_115237_addColumnTemplates
 */
class m181227_115237_addColumnTemplates extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('templates','data_footer','string(4000)' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->removeColumn('templates','data_footer');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181227_115237_addColumnTemplates cannot be reverted.\n";

        return false;
    }
    */
}
