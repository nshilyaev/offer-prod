<?php

use yii\db\Migration;

/**
 * Class m190129_162507_AddSaleValuta
 */
class m190129_162507_AddSaleValuta extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products','valuta','string DEFAULT "р."');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->removeColumn('products','isale');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190129_162507_AddSaleValuta cannot be reverted.\n";

        return false;
    }
    */
}
