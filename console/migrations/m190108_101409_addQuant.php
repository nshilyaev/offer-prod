<?php

use yii\db\Migration;

/**
 * Class m190108_101409_addQuant
 */
class m190108_101409_addQuant extends Migration
{
    /**
     * {@inheritdoc}
     */

    public function safeUp()
    {
        $this->addColumn('products','quant','integer DEFAULT 0');
        $this->addColumn('products','indsale','integer DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->removeColumn('products','quant');
        $this->removeColumn('products','indsale');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190108_101409_addQuant cannot be reverted.\n";

        return false;
    }
    */
}
